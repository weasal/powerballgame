#include "stdafx.h"
#include "powerballgame.h"

namespace Powerball {
	Game::Game(std::chrono::minutes length) : score({ { Team::Warbirds, 0 }, { Team::Javelins, 0 } }), ball(this) {
		state = State::NotStarted;
		timeStarted = std::chrono::steady_clock::now();

		this->length = length;
	}

	void Game::start() {
		if (state == State::NotStarted) {
			timeStarted = std::chrono::system_clock::now();
			state = State::InProgress;

			for (Participant& p : participants) {
				p.setActive();
			}
		}
		else {
			throw std::runtime_error("Called start() on a game that is finished or already in progress.");
		}
	}

	void Game::end() {
		if (state == State::InProgress) {
			length = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - timeStarted);
			state = State::Finished;

			for (Participant& p : participants) {
				if (p.isActive()) {
					p.setInactive();
				}
			}
		}
		else {
			throw std::runtime_error("Called end() on a game that is not in progress.");
		}
	}

	Game::State Game::getState() {
		return state;
	}

	std::unordered_map<Team, int> Game::getScore() {
		return score;
	}

	std::chrono::seconds Game::getPlayerPlaytime(std::string name, Team team) {
		Participant* participant = getParticipant(name, team);

		if (participant) {
			return participant->getPlaytime();
		}

		return std::chrono::seconds(0);
	}

	Participant* Game::getParticipant(std::string name, Team team) {
		// Strings much match perfectly!! Even length/null characters, since 
		// unordered map computes hash of key (it does not do string comparison)
		auto gotName = participantMap.find(name);
		if (gotName != participantMap.end()) {
			std::unordered_map<Team, Participant*> teamMap = gotName->second;
			auto gotTeam = teamMap.find(team);
			if (gotTeam != teamMap.end()) {
				return gotTeam->second; // found
			}
		}

		return nullptr;
	}

	Participant* Game::getActiveParticipant(std::string name) {
		Participant* birdsPart = getParticipant(name, Team::Warbirds);
		if (birdsPart && birdsPart->isActive()) {
			return birdsPart;
		}

		Participant* javsPart = getParticipant(name, Team::Javelins);
		if (javsPart && javsPart->isActive()) {
			return javsPart;
		}

		return nullptr;
	}

	std::chrono::minutes Game::getLength() {
		return std::chrono::duration_cast<std::chrono::minutes>(length);
	}


	std::chrono::time_point<std::chrono::system_clock>Game::getTimeStarted() {
		return timeStarted;
	}

	std::chrono::seconds Game::getTimePassed() {
		if (state != State::InProgress)
			return std::chrono::seconds(0);

		auto timePassed = (std::chrono::steady_clock::now() - timeStarted);
		std::chrono::milliseconds ms = std::chrono::duration_cast<std::chrono::milliseconds>(timePassed);
		// Duration cast truncates if loss of precision -- avoided below
		return std::chrono::seconds(static_cast<int>(round(ms.count() / 1000.f)));
	}

	std::list<Goal> Game::getGoals() {
		return goals;
	}

	std::list<Participant> Game::getParticipants() {
		return participants;
	}

	/**
	This function creates a Participant if necessary and stores it in a std::list. It also stores
	a reference to the participant in a std::map with the key as the pilot's name. After that it
	sets the participant as active if the game is in progress.
	*/
	void Game::logPilotEntry(std::string name, Team team) {
		// Check to see if a new participant must be added
		if (participantMap.find(name) == participantMap.end()) { // First time entering the game (doesn't exist in the map)
			// Add a new participant to the participants list
			participants.push_back(Participant(name, team));

			// Create an entry in the participants map for name
			participantMap.emplace(std::make_pair(name, std::unordered_map<Team, Participant*>()));
			participantMap[name].emplace(team, &participants.back()); // Add the participant to the new entry in the map
		}
		else { // Pilot has participated in this game already
			if (participantMap[name].find(team) == participantMap[name].end()) {
				// Pilot returning to play, but on a new team so participant added
				participants.push_back(Participant(name, team));

				participantMap[name].emplace(team, &participants.back());
			}
			else {
				// Pilot returning on an old team, participant already exists so nothing to do.
			}
		}

		// If the pilot entered a game in progress immediately activate the corresponding participant
		if (state == State::InProgress) {
			getParticipant(name, team)->setActive();
		}
	}

	void Game::logPilotExit(std::string name) {
		if (state == State::InProgress) { // If the game isn't started or is already finished no participants were/are active
			// Can infer the team
			Participant* leavingParticipant = getActiveParticipant(name);

			if (!leavingParticipant) {
				throw std::runtime_error("Logging exit for pilot that had never been logged entering.");
			}
			else {
				leavingParticipant->setInactive();
			}
		}
		else if (state == State::NotStarted) {
			/* With this omitted, problems occur when a player leaves a game that has yet to start
			i.e. PowerballGame is created, has not started, logPilotEntry("Crescendo"), logPilotExit("Crescendo")
			then start() will result in a Participant named Crescendo that will be active for the entire game (even though
			they exited before the game started). */

			// Remove all participants named name from the game
			// Important note: modifying std::list participants does not affect any pointers to a Participant in 
			// the std::unordered_map<...> participantsMap
			participants.remove_if([name](Participant& p) {
				return p.getName() == name;
			});

			participantMap.erase(name);

#ifdef _DEBUG
			/* Check to make sure all references in participantMap are still valid (despite above note -- as a precaution) */
			for (Participant& part : participants) {
				auto mapPart = getParticipant(part.getName(), part.getTeam()); // retrieve same participant using participantsMap
				mapPart->setInactive(); // Dummy actions
				mapPart->getPlaytime();
			}
#endif
		}
	}

	void Game::logPilotTeamSwitch(std::string name, Team newTeam) {
		// Treated internally as an exit, then entry on other team
		Team oldTeam = (newTeam == Team::Warbirds) ? Team::Javelins : Team::Warbirds;
		Participant* currentParticipant = getParticipant(name, oldTeam);

		if (currentParticipant) {
			logPilotExit(name);
			logPilotEntry(name, newTeam);
		}
		else {
			throw std::runtime_error("Logging team switch for pilot that had never been logged entering.");
		}
	}

	/* Goalies are the enemy pilots that were in the goal to defend the shot around the time of scoring */
	void Game::logGoal(Team team, std::list<std::string> goalies) {
		if (state != State::InProgress)
			return;

		score[team] = score[team] + 1;

		// Get last ball carrier, could also get the scorer from the arena message
		Participant* scorer = ball.getCarrier();
		scorer->logStat(Participant::Stat::Goal);

		Participant* passer = ball.getCarrier(1);
		std::string assister;
		if (passer && passer->getTeam() == scorer->getTeam()) {
			passer->logStat(Participant::Stat::Assist);
			assister = passer->getName();
		}

		Team otherTeam = (team == Team::Warbirds) ? Team::Javelins : Team::Warbirds;
		for (std::string goalie : goalies) {
			Participant* defender = getParticipant(goalie, otherTeam);
			defender->logStat(Participant::Stat::GoalAllowed);
		}

		Goal goal = {};
		goal.team = team;
		goal.scorer = scorer->getName();
		goal.assister = assister;
		goal.goalies = goalies;
		goal.scoredAt = std::chrono::system_clock::now();
		goal.shotAt = ball.getLastCarriedTime();
		goal.trajectory = ball.getLastTrajectory();
		goal.carriedFor = std::chrono::duration_cast<std::chrono::milliseconds>(ball.getLastCarriedTime() - ball.getLastCaughtTime());
		goals.push_back(goal);
	}

	// carrier == empty string signifies ball is uncarried (no carrier)
	void Game::logBallUpdate(std::string carrier, bool isInOwnGoal, double x, double y, double vx, double vy) {
		if (state != State::InProgress)
			return;

		BallTracker::UpdateInfo update = ball.update(carrier, x, y, vx, vy);

		if (ball.isCarried()) {
			Participant* ballCarrier = getActiveParticipant(carrier);

			if (!ballCarrier)
				throw std::runtime_error("Couldn't find participant for ball carrier");

			if (update.wasCaughtOrPickedUp) {
				// true if ball was caught or picked up on this update, false if it is same state
				// as before (uncarried, or carried by same pilot)
				ballCarrier->logStat(Participant::Stat::BallCarry);

				if (update.changedPossession) {
					if (isInOwnGoal) {
						// Catches in the carrier's own goal are saves, and aren't considered turnovers for the other pilot
						// (note own goal is important because otherwise it would count a save for an enemy pilot that 
						// receives a turnover in the attacking goal -- e.g. if ball stolen from goalie)
						ballCarrier->logStat(Participant::Stat::Save);
					}
					else {
						ballCarrier->logStat(Participant::Stat::Steal);

						// Need to use getParticipant and not getActiveParticipant because the fumbler
						// may be inactive (i.e. have specced after losing the ball but before another pilot picked it up)
						Participant* fumbler = ball.getCarrier(1);
						fumbler->logStat(Participant::Stat::Turnover);
					}
				}
			}
		}
	}

	// amount of time in milliseconds for a kill on the last ball carrier to be considered a pressure
#define PRESSURE_THRESHOLD 500 
	void Game::logKill(std::string killer, std::string killed) {
		if (state != State::InProgress)
			return;

		Participant* participantKiller = getActiveParticipant(killer);
		Participant* participantKilled = getActiveParticipant(killed);
		if (participantKiller && participantKilled) {
			participantKiller->logStat(Participant::Stat::Kill);
			participantKilled->logStat(Participant::Stat::Death);

			/* Check for a pressure
			Pressure is a concept used because it's not reliably known whether killed players carry the ball at
			the time of their death. So cannot track BallKills because that's sensitive to:
			<server player death>, Event_BallMove**, Event_PlayerDeath, or
			<server player death>, Event_PlayerDeath, Event_BallMove**

			**When the BallMove event is received after the player dies on server then isCarried() will be false. */
			bool wasPressure = false;
			// Check getCarrier() is not null (it is null until ball is picked up for the first time after game start)
			bool killedBall = ball.getCarrier() && (ball.getCarrier()->getName() == killed);

			if (ball.isCarried()) {
				if (killedBall) // In this case (since isCarried = true), the killed pilot carried the ball
					wasPressure = true;
			}
			else if (killedBall) { // The killed pilot did not have the ball, but was the last ball carrier
				std::chrono::milliseconds timeAgoCarried =
					std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - ball.getLastCarriedTime());
				if (timeAgoCarried.count() <= PRESSURE_THRESHOLD)
					wasPressure = true;
			}

			if (wasPressure) {
				participantKiller->logStat(Participant::Stat::Pressure);
			}
		}
	}

#define OUTPUT_NAME_LENGTH 16
	std::list<std::string> Game::getResultsOutput() {
		std::list<std::string> output;

		std::stringstream scoreStream;

		/* This block = "Final Score: <birds score>-<javelins-score> <warbirds|javelins> win" */
		scoreStream << "Final Score: " << score[Team::Warbirds] << "-" << score[Team::Javelins];
		if (score[Team::Warbirds] == score[Team::Javelins])
			scoreStream << " Tie";
		else
			scoreStream << ((score[Team::Warbirds] > score[Team::Javelins]) ? " Warbirds win" : " Javelins win");
		output.push_back(scoreStream.str());

		/* Get lists of each team's participants */
		std::list<Participant*> warbirds, javelins;
		for (Participant& participant : participants) {
			if (participant.getTeam() == Team::Warbirds)
				warbirds.push_back(&participant);
			else
				javelins.push_back(&participant);
		}

		/* Sort the participants by the total time played */
		std::function<bool(Participant*, Participant*)> comparePlaytime = [](Participant* a, Participant* b) {
			return a->getPlaytime().count() > b->getPlaytime().count();
		};
		warbirds.sort(comparePlaytime);
		javelins.sort(comparePlaytime);

		output.push_back("+------------------+---------------------+----------------+----------------+--------+");
		output.push_back("| Team: Warbirds   |    G   As    S   Ga |   Bc   St   To |    P    K    D |     Pt |");
		output.push_back("+------------------+---------------------+----------------+----------------+--------+");

		std::unordered_map<Participant::Stat, int> warbirdTotals;
		bool firstEntry = true;

		for (Participant* bird : warbirds) {
			std::stringstream birdStats;
			birdStats << "| ";
			std::string name = bird->getName();
			int limit = (name.size() < OUTPUT_NAME_LENGTH) ? name.size() : OUTPUT_NAME_LENGTH;
			std::string truncatedName = bird->getName().substr(0, limit);
			birdStats << std::setw(OUTPUT_NAME_LENGTH) << std::left << std::setfill(' ') << truncatedName;
			birdStats << " |";

			for (Participant::Stat stat : Participant::STATS) {
				if (stat == Participant::Stat::BallCarry || stat == Participant::Stat::Pressure)
					birdStats << " |";

				birdStats << std::setw(5) << std::setfill(' ') << std::right << bird->getStat(stat);

				if (firstEntry)
					warbirdTotals[stat] = bird->getStat(stat);
				else
					warbirdTotals[stat] += bird->getStat(stat);
			}

			birdStats << " | ";
			int playtime = static_cast<int>(bird->getPlaytime().count()); // total playtime in seconds
			int minutes = playtime / 60;
			int seconds = playtime % 60;
			birdStats << std::setw(3) << std::setfill(' ') << std::right << minutes;
			birdStats << ":";
			birdStats << std::setw(2) << std::setfill('0') << seconds;
			birdStats << " |";

			output.push_back(birdStats.str());

			firstEntry = false;
		}

		std::stringstream birdsTotalLine;
		birdsTotalLine << "|  Total           |"; // Indent one space
		for (Participant::Stat stat : Participant::STATS) {
			if (stat == Participant::Stat::BallCarry || stat == Participant::Stat::Pressure)
				birdsTotalLine << " |";

			birdsTotalLine << std::setw(5) << std::setfill(' ') << std::right << warbirdTotals[stat];
		}
		birdsTotalLine << " |        |";
		output.push_back(birdsTotalLine.str());

		output.push_back("+==================+=====================+================+================+========+");
		output.push_back("| Team: Javelins   |    G   As    S   Ga |   Bc   St   To |    P    K    D |     Pt |");
		output.push_back("+------------------+---------------------+----------------+----------------+--------+");

		std::unordered_map<Participant::Stat, int> javelinTotals;
		firstEntry = true;

		for (Participant* jav : javelins) {
			std::stringstream javStats;
			javStats << "| ";
			std::string name = jav->getName();
			int limit = (name.size() < OUTPUT_NAME_LENGTH) ? name.size() : OUTPUT_NAME_LENGTH;
			std::string truncatedName = jav->getName().substr(0, limit);
			javStats << std::setw(OUTPUT_NAME_LENGTH) << std::left << std::setfill(' ') << truncatedName;
			javStats << " |";

			for (Participant::Stat stat : Participant::STATS) {
				if (stat == Participant::Stat::BallCarry || stat == Participant::Stat::Pressure)
					javStats << " |";

				javStats << std::setw(5) << std::setfill(' ') << std::right << jav->getStat(stat);

				if (firstEntry)
					javelinTotals[stat] = jav->getStat(stat);
				else
					javelinTotals[stat] += jav->getStat(stat);
			}

			javStats << " | ";
			int playtime = static_cast<int>(jav->getPlaytime().count()); // total playtime in seconds
			int minutes = playtime / 60;
			int seconds = playtime % 60;
			javStats << std::setw(3) << std::setfill(' ') << std::right << minutes;
			javStats << ":";
			javStats << std::setw(2) << std::setfill('0') << seconds;
			javStats << " |";

			output.push_back(javStats.str());

			firstEntry = false;
		}

		std::stringstream javsTotalLine;
		javsTotalLine << "|  Total           |"; // Indent one space
		for (Participant::Stat stat : Participant::STATS) {
			if (stat == Participant::Stat::BallCarry || stat == Participant::Stat::Pressure)
				javsTotalLine << " |";

			javsTotalLine << std::setw(5) << std::setfill(' ') << std::right << javelinTotals[stat];
		}
		javsTotalLine << " |        |";
		output.push_back(javsTotalLine.str());

		output.push_back("+------------------+---------------------+----------------+----------------+--------+");

		return output;
	}
}