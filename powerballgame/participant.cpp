#include "stdafx.h"
#include "powerballgame.h"

namespace Powerball {
	// List of stats that can be used to iterate over them (i.e. for (Stat stat: STATS_LIST) { ... }
	const std::list<Participant::Stat> Participant::STATS({
		Participant::Stat::Goal,			// Game::logGoal
		Participant::Stat::Assist,			// Game::logGoal
		Participant::Stat::Save,			// Game::logBallUpdate
		Participant::Stat::GoalAllowed,		// Game::logGoal
		Participant::Stat::BallCarry,		// Game::logBallUpdate
		Participant::Stat::Steal,			// Game::logBallUpdate
		Participant::Stat::Turnover,		// Game::logBallUpdate
		Participant::Stat::Pressure,		// Game::logKill
		Participant::Stat::Kill,			// Game::logKill
		Participant::Stat::Death			// Game::logKill
	});

	Participant::Participant(std::string name, Team team) {
		this->name = name;
		this->team = team;

		active = false;

		for (Stat stat : STATS) {
			stats[stat] = 0;
		}
	}

	std::string Participant::getName() {
		return name;
	}

	Team Participant::getTeam() {
		return team;
	}

	std::chrono::seconds Participant::getPlaytime() {
		std::chrono::seconds playTime;

		for (auto iter = activeTimes.begin(); iter != activeTimes.end(); iter++) {
			auto startEndTimePair = *iter;
			auto duration = startEndTimePair.second - startEndTimePair.first;
			playTime += std::chrono::duration_cast<std::chrono::seconds>(duration);
		}

		if (active) {
			playTime += std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - lastActivatedAt);
		}

		return playTime;
	}

	std::list < std::pair < std::chrono::time_point<std::chrono::system_clock>,
		std::chrono::time_point<std::chrono::system_clock >> > Participant::getActiveTimes() {

		if (active) {
			auto duplicate = activeTimes;
			duplicate.emplace_back(lastActivatedAt, std::chrono::system_clock::now());
		}

		return activeTimes;
	}

	bool Participant::isActive() {
		return active;
	}

	void Participant::setActive() {
		if (!active) {
			active = true;
			lastActivatedAt = std::chrono::system_clock::now();
		}
	}

	void Participant::setInactive() {
		if (active) {
			active = false;
			activeTimes.emplace_back(lastActivatedAt, std::chrono::system_clock::now());
		}
	}

	void Participant::logStat(Stat stat) {
		stats[stat] = stats[stat] + 1;
	}

	int Participant::getStat(Stat stat) {
		return stats[stat];
	}
}