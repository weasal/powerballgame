#include "stdafx.h"
#include "powerballgame.h"

#include <algorithm> // for std::reverse

#include <boost\geometry.hpp> 
#include <boost\geometry\geometries\point.hpp>
#include <boost\geometry\geometries\geometries.hpp>
#include <boost\geometry\algorithms\equals.hpp>

#define POSITION_HISTORY_LIMIT 50
#define CARRIER_HISTORY_LIMIT 10

// The minimum change in ball distance and direction to count as an actual change (i.e. bounce)
#define DISTANCE_CHANGE_THRESHOLD 1.f
#define DIRECTION_CHANGE_THRESHOLD (0.5 * 3.14159265359 / 180)

namespace Powerball {
	Game::BallTracker::BallTracker(Game* game) {
		this->game = game;

		carried = false;

		lastDirection = 0; // direction in radians;
		lastVelocity = model::d2::point_xy<double>(0.f, 0.f);
		lastPosition = model::d2::point_xy<double>(0.f, 0.f);
	}

	Game::BallTracker::UpdateInfo Game::BallTracker::update
		(std::string carrierName, double x, double y, double vx, double vy) {

		UpdateInfo info = { 0 };
		model::d2::point_xy<double> position(x, y);
		model::d2::point_xy<double> velocity(vx, vy);
		double direction;
		if (vx != 0 || vy != 0)
			direction = atan2(vy, vx);
		else
			direction = 0;

		/* Ball not being carried */
		if (carrierName.empty()) {
			if (carried) {
				// Ball was shot/dropped
				lastCarryPosition = position;
				positions.push_back(lastCarryPosition);
				lastCarriedAt = std::chrono::steady_clock::now();
				carried = false;
			}
			else {
				/* Filter the position updates so that only the end points of the path
				are stored */
				double dist = distance(position, lastPosition);
				// direction ball is headed in radians
				if (vx != 0 || vy != 0) {
					if (dist > DISTANCE_CHANGE_THRESHOLD &&  abs(direction - lastDirection) > DIRECTION_CHANGE_THRESHOLD) {
						// get the intersection between lastPosition, lastVelocity, and new position, new velocity

						// Extrapolate to segments using velocity
						model::segment<model::d2::point_xy<double>> lastPath(lastPosition,
							model::d2::point_xy<double>(lastPosition.x() + 2 * lastVelocity.x(), lastPosition.y() + 2 * lastVelocity.y()));
						model::segment<model::d2::point_xy<double>> currentPath(position, model::d2::point_xy<double>(x - 2 * vx, y - 2 * vy));

						std::vector<model::d2::point_xy<double>> output;

						intersection(lastPath, currentPath, output);

						if (output.empty())
							positions.push_back(lastPosition);
						else
							positions.push_back(output[0]);
					}
				}
				else {
					// Ball is not moving
					if (!equals(position, lastPosition)) {
						// Not interested in non-moving ball positions however (only shots/passes), so ignore
						//positions.push_back(position);
					}
				}
			}

			if (positions.size() > POSITION_HISTORY_LIMIT)
				positions.pop_front();
		}
		else {
			if (!carried) { // If it wasn't carried previously, then it's been picked up or caught
				info.wasCaughtOrPickedUp = true;
				pickedUpAt = std::chrono::steady_clock::now();
				carried = true;
			}

			Participant* current = game->getActiveParticipant(carrierName);

			if (current == nullptr) {
				throw std::runtime_error("Could not find participant for ball carrier.");
			}

			if (carriers.size() == 0) { // First carrier in the game
				carriers.push_back(current);
			}
			else {
				Participant* last = carriers.back();

				if (current != last) { // Check if the current carrier is not the same as previous
					info.changedCarrier = true;
					if (current->getTeam() != last->getTeam())
						info.changedPossession = true;

					carriers.push_back(current);
				}
			}

			// Make sure the size of the carrier list doesn't exceed the limit
			if (carriers.size() > CARRIER_HISTORY_LIMIT)
				carriers.pop_front();
		}

		lastPosition = position;
		lastVelocity = velocity;
		lastDirection = direction;

		return info;
	}

	/* Position 0 returns the player who has most recently had the ball,
	i.e. this is the current (if isCarried) or last (if !isCarried) pilot with the ball.
	Position 1 returns the previous ball carrier, 2 the carrier previous to that, and so on... */
	Participant* Game::BallTracker::getCarrier(int position) {
		int last = carriers.size() - 1;
		int index = last - position;

		if (index >= 0) {
			return (carriers[index]);
		}

		return nullptr;
	}

	bool Game::BallTracker::isCarried() {
		return carried;
	}


	std::chrono::time_point<std::chrono::system_clock> Game::BallTracker::getLastCaughtTime() {
		return pickedUpAt;
	}

	std::chrono::time_point<std::chrono::system_clock> Game::BallTracker::getLastCarriedTime() {
		return lastCarriedAt;
	}

	std::vector<model::d2::point_xy<double>> Game::BallTracker::getLastTrajectory() {
		if (positions.size() <= 1)
			return std::vector<model::d2::point_xy<double>>({ lastCarryPosition });

		std::vector<model::d2::point_xy<double>> trajectory;

		for (auto riter = positions.rbegin(); !equals(*riter, lastCarryPosition); riter++) {
			trajectory.push_back(*riter);
		}

		trajectory.push_back(lastCarryPosition);
		std::reverse(trajectory.begin(), trajectory.end());

		if (!equals(trajectory.back(), lastPosition))
			trajectory.push_back(lastPosition);

		return trajectory;
	}
}